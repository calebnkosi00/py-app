# py-app
This is a simple django app which allows user/client to register a profile !


## Getting started
The app is deploy through gitlab-ci pipeline, 

## Prerequisites
To successfully deploy the application on aws ec2 ubuntu instance you will need the following environment variables and set them on gitlab variables.
- The private key of the ec2 instance $PRIVATE_KEY which is the private.pem file from terraform
- The remote public ip address of the ec2 instance `$REMOTE_IP`
- The remote user for the ec2 instance `$REMOTE_USER`


## Run Application
If the environment variables are set correctly, on the gitlab environment variables you can trgigger the pipeline.
The pipeline consist of two stages:
- [] build
- [] deploy
- The build job only creates the docker images and store them on the gitlab [registery](https://gitlab.com/calebnkosi00/py-app/container_registry)
- The deploy job deploys the application on aws ec2 instance.

- To Run locally
```
$ docker-compose up -d --build
```
- [] Test app [http://localhost/](http://localhost/)

## Access Application
- To access the app [http://$REMOTE_IP](http://$REMOTE_IP)
- Admin credentials username `caleb1` and password `jc1992`
- To access admin dashboard [http://$REMOTE_IP/dashboarddashboard](http://$REMOTE_IP/dashboard)  you need to first login in the app [http://$REMOTE_IP](http://$REMOTE_IP) with admin credentials provided above then use  [http://$REMOTE_IP/dashboard](http://$REMOTE_IP/dashboard) to track your users/clients

