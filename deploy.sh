#!/bin/sh

ssh -o StrictHostKeyChecking=no $REMOTE_USER@$REMOTE_IP<< 'ENDSSH'
  sudo apt-get update -y
  
  sudo apt-get install -y docker.io 
  sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" \
    -o /usr/local/bin/docker-compose
  sudo chmod +x /usr/local/bin/docker-compose
  sudo chmod 666 /var/run/docker.sock
  sleep 10
  sudo systemctl start docker
  sudo usermod -aG docker $USER 

  export IMAGE=$CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME
  export APP_IMAGE=$IMAGE:py-app_django_gunicorn
  export NGINX_IMAGE=$IMAGE:py-app_nginx
  cd /home/ubuntu/app
  export $(cat .env | xargs)
  docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
  docker-compose -f docker-compose.yml down
  docker pull $NGINX_IMAGE
  docker pull $APP_IMAGE
  docker-compose -f docker-compose.yml up -d 
ENDSSH
