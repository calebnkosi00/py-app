from django.shortcuts import render, redirect 
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import get_user_model
from django.contrib.auth import authenticate, login, logout

from django.contrib.auth.models import Group
from django.contrib import messages
from .decorators import allowed_users, admin_only
from django.contrib.auth.decorators import login_required

# Create your views here.
from .models import *
from .forms import SignUpForm


def home(request):
    context = {}
    return render(request, 'app/home.html')
@login_required(login_url='login')
@admin_only
def dashboard(request):
    User = get_user_model()
    users = User.objects.all()
    context = {'users': users}
    return render(request, 'app/admin-portal.html', context)

@login_required(login_url='login')
def profile(request):
    context = {}
    return render(request, 'app/profile.html')

def register(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get('username')
            group = Group.objects.get(name='users')
            user.groups.add(group)
            return redirect('login')
    else:
        form = SignUpForm()
    return render(request, 'app/register.html', {'form': form})

def loginPage(request):

	if request.method == 'POST':
		username = request.POST.get('username')
		password =request.POST.get('password')

		user = authenticate(request, username=username, password=password)

		if user is not None:
			login(request, user)
			return redirect('profile')
		else:
			messages.info(request, 'Username OR password is incorrect')

	context = {}
	return render(request, 'app/login.html', context)

def logoutPage(request):
	logout(request)
	return redirect('login')
